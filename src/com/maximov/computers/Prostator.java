package com.maximov.computers;

import com.maximov.consumer.Consumer;

public class Prostator extends Computer {
    public Prostator(int[] numbers, Consumer consumer) {
        super(numbers, consumer);
    }

    @Override
    protected boolean isConsumerBusy() {
        return this.consumer.isBusyProstos;
    }

    @Override
    protected int compute(int number){
        return number;
    }

    @Override
    protected void callConsumer(int result){
        this.consumer.mes(0, 0, result);
    }
}
