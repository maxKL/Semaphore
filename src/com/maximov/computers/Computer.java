package com.maximov.computers;

import com.maximov.consumer.Consumer;

public abstract class Computer implements Runnable {
    protected int[] numbers;
    protected Consumer consumer;

    protected Computer(int[] numbers, Consumer consumer){
        this.numbers = numbers;
        this.consumer = consumer;

    }

    @Override
    public void run() {
        for(int i = 0; i < this.numbers.length;i++){
            while (isConsumerBusy()) {}
            callConsumer(compute(this.numbers[i]));
        }
    }

    protected abstract int compute(int number);

    protected abstract void callConsumer(int number);

    protected abstract boolean isConsumerBusy();
}
