package com.maximov.computers;

import com.maximov.consumer.Consumer;

public class Kvadrator extends Computer {

    public Kvadrator(int[] numbers, Consumer consumer) {
        super(numbers, consumer);
    }

    @Override
    protected boolean isConsumerBusy() {
        return this.consumer.isBusyKvadro;
    }

    @Override
    protected int compute(int number){
        return number * number;
    }

    @Override
    protected void callConsumer(int result){
        this.consumer.mes(0, result, 0);
    }
}
