package com.maximov.computers;

import com.maximov.consumer.Consumer;

public class Kubator extends Computer {

    public Kubator(int[] numbers, Consumer consumer) {
        super(numbers, consumer);
    }

    @Override
    protected boolean isConsumerBusy() {
        return this.consumer.isBusyKube;
    }

    @Override
    protected int compute(int number){
        return number * number * number;
    }

    @Override
    protected void callConsumer(int result){
        this.consumer.mes(result, 0, 0);
    }
}
