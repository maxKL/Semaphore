package com.maximov.consumer;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

public class Consumer {
    private LinkedList<Integer> kubes = new LinkedList<>();
    private LinkedList<Integer> kvadros = new LinkedList<>();
    private LinkedList<Integer> prostos = new LinkedList<>();
    public volatile boolean isBusyKube;
    public volatile boolean isBusyKvadro;
    public volatile boolean isBusyProstos;
    private AtomicBoolean areListsSumming= new AtomicBoolean(false);

    public void mes(int kube, int kvadrat, int chislo){
        if(kube != 0){


            this.isBusyKube = true;
            kubes.add(kube);
            showSum();
            this.isBusyKube = false;
            return;


        }

        if(kvadrat != 0){
            this.isBusyKvadro = true;
            kvadros.add(kvadrat);
            showSum();
            this.isBusyKvadro = false;
            return;
        }

        if(chislo != 0){
            this.isBusyProstos = true;
            prostos.add(chislo);
            showSum();
            this.isBusyProstos = false;
            return;
        }
    }

    private void showSum(){
        while (areListsSumming.get()) {}
        areListsSumming.compareAndSet(false,true);
        if (kubes.size() > 0 && kvadros.size() > 0 && prostos.size() > 0) {
            System.out.println(kubes.get(0) + kvadros.get(0) + prostos.get(0));
            kubes.remove(0);
            kvadros.remove(0);
            prostos.remove(0);
        }
        areListsSumming.compareAndSet(true,false);
    }
}
