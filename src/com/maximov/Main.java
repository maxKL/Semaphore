package com.maximov;

import com.maximov.computers.*;
import com.maximov.consumer.Consumer;

public class Main {

    public static void main(String[] args) {
	    int[] dataA = { 1, 2, 3};
	    int[] dataB = { 4, 5, 6};
	    int[] dataC = { 7, 8, 9};

        Consumer consumer = new Consumer();
        new Thread(new Kubator(dataA, consumer)).start();
        new Thread(new Kvadrator(dataB, consumer)).start();
        new Thread(new Prostator(dataC, consumer)).start();
    }
}
